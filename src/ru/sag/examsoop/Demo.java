package ru.sag.examsoop;

import java.util.Scanner;

public class Demo {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println(inputButton());
        System.out.println(inputTextButton());
    }

    private static Button inputButton() {
        System.out.print("Введите ширину: ");
        int width = sc.nextInt();
        System.out.print("Введите высоту: ");
        int height = sc.nextInt();
        return new Button(width, height);
    }


    private static TextButton inputTextButton() {
        System.out.println("Введите текст: ");
        String text = sc.next();
        System.out.print("Введите ширину: ");
        int width = sc.nextInt();
        System.out.print("Введите высоту: ");
        int height = sc.nextInt();
        return new TextButton(width, height, text);
    }
}
