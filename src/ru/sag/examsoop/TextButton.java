package ru.sag.examsoop;

public class TextButton extends Button {
private String text;

    TextButton(int width, int height, String text) {
        super(width, height);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Текстовая кнопка (" +
                "текст = " + text +
                ", ширина = " + getWidth() +
                ", высота = " + getHeight() +
                ')';
    }
}
