package ru.sag.examsoop;

public class Button {
    private int width;
    private int height;

    Button(int width, int height) {
        this.width = width;
        this.height = height;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Кнопка (" +
                "ширина = " + width +
                ", высота = " + height +
                ')';
    }
}
