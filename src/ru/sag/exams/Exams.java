package ru.sag.exams;

public class Exams {
    public static void main(String[] args) {
        for (int carat = 1; carat < 10; carat = carat + 2) {
            double grams = carat * 0.2;
            System.out.println("В каратах:" + " " + carat + " = " + " в граммах:" + " " + grams);
        }
    }
}
